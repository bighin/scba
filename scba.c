/*
	scba.c

	Self-consistent Born approximation for the angulon
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>

/*
	We use the Cubature library for numerical integration,
	see http://ab-initio.mit.edu/wiki/index.php/Cubature
*/

#include "cubature-1.0.2/cubature.h"

/*
	Nice progress bar
*/

#include "libprogressbar/progressbar.h"

#include "selfenergies.h"
#include "spline.h"

/*
	To receive push notifications on your iPhone when a task is done
*/

#include <curl/curl.h>
#include "cpushover/cpushover.h"

/*
    GCC does not define M_PI in C11 mode
*/

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif

/*
	Tuning paramaters for numerical integration.
*/

size_t maxEval=32*1024;
double relError=1e-4;

/*
	The small imaginary part added to calculate the retarded propagator.
*/

double epsilon=1e-1;

/*
	Spline interpolation, given a grid of points, essentially a nice wrapper
	over the code contained in Numerical Recipes (NR).

	Note that the code in NR is __not__ thread safe, the static keyword has to
	be removed in the splint() function.
*/

struct interpolation_t
{
	double *x;
	double *y;

	int n;
	
	double *y2;
};

struct interpolation_t *init_interpolation(double *x,double *y,int n)
{
	struct interpolation_t *ret;
	
	if(!(ret=malloc(sizeof(struct interpolation_t))))
		return NULL;

	ret->x=malloc(sizeof(double)*n);
	ret->y=malloc(sizeof(double)*n);
	ret->y2=malloc(sizeof(double)*n);

	if((!ret->x)||(!ret->y)||(!ret->y2))
		return NULL;

	memcpy(ret->x,x,sizeof(double)*n);
	memcpy(ret->y,y,sizeof(double)*n);
	ret->n=n;
	
	spline(ret->x,ret->y,ret->n,0.0f,0.0f,ret->y2);

	return ret;
}

void fini_interpolation(struct interpolation_t *it)
{
	if(it)
	{
		if(it->x)
			free(it->x);

		if(it->y)
			free(it->y);

		if(it->y2)
			free(it->y2);
	
		free(it);
	}
}

double get_point(struct interpolation_t *it,double x)
{
	double y;
	
	splint(it->x,it->y,it->y2,it->n,x,&y);
	
	return y;
}

/*
	Main parameters: highest angular momentum, numbers of elements in the grid.
*/

#define MAXL 		(4)
#define DIVS		(1000)

/*
	Anderson parameter
*/

#define ALPHA		(1.0f)

/*
	The function defining Born approximation itself
*/

struct born_ctx_t
{
	double complex omega;
	double n,energy_cutoff;
	
	short lambda,lambda1,lambda2;

	struct interpolation_t **sigma_interpolations_r,**sigma_interpolations_i;
};

int fBorn(unsigned ndim,const double *x,void *fdata,unsigned fdim,double *fval)
{
	/* The context from which we read the global variables */
	
	struct born_ctx_t *ctx=(struct born_ctx_t *)(fdata);

	/* Global variables */

	short lambda1,lambda2;
	double omega,n;
	double complex sigma;

	/* The integration variables */

	double t=x[0];
	double k=t/(1.0f-t);
	
	double omegaprime=x[1];

	/* Auxiliary variables */

	double complex a,z;

	/* We load the global variables */

	omega=ctx->omega;
	n=ctx->n;

	lambda1=ctx->lambda1;
	lambda2=ctx->lambda2;

	/* Finally, we compute the result and return it */

	if(fabs(omega-omegaprime)<ctx->energy_cutoff)
		sigma=get_point(ctx->sigma_interpolations_r[lambda1],omega-omegaprime)+I*get_point(ctx->sigma_interpolations_i[lambda1],omega-omegaprime);
	else
		sigma=Sigma1(lambda1,omega-omegaprime,n);

	a=1.0f/(omega-omegaprime-lambda1*(lambda1+1)-sigma+I*epsilon);
	a*=1.0f/(omegaprime-omegak(k,n)+I*epsilon);

	a*=-I/(2.0f*M_PI);

	z=pow(U(lambda2,n,k),2.0f)*a*pow(1-t,-2.0f);

	fval[0]=creal(z);
	fval[1]=cimag(z);

	return 0;
}

void born_iteration(short L,double omega,double n,struct interpolation_t **sigma_interpolations_r,struct interpolation_t **sigma_interpolations_i,
                    double energy_cutoff,double complex *sigma,double complex *esigma)
{
	struct born_ctx_t ctx;
	double complex total=0.0f,etotal=0.0f;

	ctx.lambda=L;
	ctx.n=n;
	ctx.omega=omega;
	ctx.energy_cutoff=energy_cutoff;

	ctx.sigma_interpolations_r=sigma_interpolations_r;
	ctx.sigma_interpolations_i=sigma_interpolations_i;

	for(ctx.lambda2=0;ctx.lambda2<=1;ctx.lambda2++)
	{
		for(ctx.lambda1=0;ctx.lambda1<MAXL;ctx.lambda1++)
		{
			double w3j=wigner3j(L,ctx.lambda1,ctx.lambda2,0,0,0);
		
			if(fabs(w3j)>1e-7)
			{
				double y=-(2*ctx.lambda1+1)*(2.0f*ctx.lambda2+1)/(4.0f*M_PI);

				double xmin[2]={0.0f,-energy_cutoff};
				double xmax[2]={1.0f,energy_cutoff};
				double res[2],err[2];

				hcubature(2,fBorn,&ctx,2,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);

				total+=y*pow(w3j,2.0f)*(res[0]+I*res[1]);
				etotal+=y*pow(w3j,2.0f)*(err[0]+I*err[1]);
			}
		}
	}

	*sigma=total;
	*esigma=etotal;
}


/*
	This function defines an uniformly-spaced grid.
*/

double get_omega_uniform(int c,double energy_cutoff)
{
	/*
		step is he smallest omega division, defining the grid.
	*/

	double step=(2.0f*energy_cutoff)/DIVS;

	return -energy_cutoff+step*c;
}

/*
	This function defines a variably-spaced grid.

	Given a number DIVS of division, the integration range [-energy_cutoff,+energy_cutoff]
	is divided in three areas:

	A = [-energy_cutoff,-energy_cutoff/5.0]
	B = [-energy_cutoff/5.0,energy_cutoff/5.0]
	C = [energy_cutoff/5.0,energy_cutoff]

	Half of the grid points will be in B, while the other half will be on A and B combined.
	This results in a grid which is 4 times finer in the B region.
*/

double get_omega_variable(int c,double energy_cutoff)
{
	int d4;
	double omega0,omega1,omega2,smallstep,bigstep;
	
	assert((DIVS%4)==0);

	d4=DIVS/4;

	omega0=-energy_cutoff;
	omega1=-energy_cutoff/5.0f;
	omega2=energy_cutoff/5.0f;

	bigstep=fabs(omega1-omega0)/d4;
	smallstep=fabs(omega2-omega1)/(2.0f*d4);

	if(c<d4)
		return omega0+bigstep*c;

	if(c<3*d4)
		return omega1+smallstep*(c-d4);

	return omega2+bigstep*(c-3*d4);
}

double get_omega(int c,double energy_cutoff)
{
	return get_omega_variable(c,energy_cutoff);
}

void dump_tables(char *fname,double energy_cutoff,int iteration,double n,
                 struct interpolation_t *sigma_interpolations_r[MAXL],struct interpolation_t *sigma_interpolations_i[MAXL])
{
	FILE *out;
	
	if(!(out=fopen(fname,"w+")))
		return;
	
	fprintf(out,"# SCBA\n");
	fprintf(out,"# Energy cutoff: %f\n",energy_cutoff);
	fprintf(out,"# Grid divisions: %i\n",DIVS);
	fprintf(out,"# Step size: variable\n");
	fprintf(out,"# Iteration: %i\n",iteration);
	fprintf(out,"# Anderson parameter (alpha): %f\n",ALPHA);
	fprintf(out,"# Density, log(n): %f\n",log(n));
	fprintf(out,"#\n");
	fprintf(out,"# log(n) omega Re(Sigma(L=0)) Im(Sigma(L=0)) ...  Re(Sigma(L=MAXL)) Im(Sigma(L=MAXL))\n");

	for(int c=0;c<DIVS;c++)
	{
		int d;

		double omega=get_omega(c,energy_cutoff);

		fprintf(out,"%f %f ",log(n),omega);
	
		for(d=0;d<MAXL;d++)
		{
			fprintf(out,"%f %f ",get_point(sigma_interpolations_r[d],omega),get_point(sigma_interpolations_i[d],omega));
		}

		fprintf(out,"\n");
	}

	if(out)
		fclose(out);
}

/*
	An utility function to copy f1 to f2.

	Courtesy of Jonathan Leffler, from http://stackoverflow.com/questions/1006797/tried-and-true-simple-file-copying-code-in-c
*/

#define BUFSIZE	1024

void fcopy(FILE *f1,FILE *f2)
{
	char buffer[BUFSIZE];
	size_t n;

	while((n=fread(buffer,sizeof(char),sizeof(buffer),f1))>0)
	{
		if(fwrite(buffer,sizeof(char),n,f2)!=n)
			fprintf(stderr,"Error: write failed.\n");
	}
}

int scba(double n,int max_iterations,double energy_cutoff,char *prefix,FILE *logfile)
{
	/*
		A grid containing the values of \Sigma, from which we derive the interpolations.
	*/

	double sigmas_r[MAXL][DIVS],sigmas_i[MAXL][DIVS];
	double grid[DIVS];
	
	/*
		The interpolations themselves
	*/

	struct interpolation_t *sigma_interpolations_r[MAXL];
	struct interpolation_t *sigma_interpolations_i[MAXL];

	/*
		Auxiliary variables.
	*/

	int c,i;

	/*
		Should we exit the main loop earlier?
	*/

	short earlyexit=0;

	/*
		The index of the last iteration, which may not be max_iterations
		in case of an early exit, as determined by achieved convergence.
	*/

	int last_iteration=0;

	/*
		We fill the grids, using the one-loop selfenergy as initial guess...
	*/

	for(c=0;c<DIVS;c++)
	{
		int d;

		double omega=get_omega(c,energy_cutoff);

		for(d=0;d<MAXL;d++)
		{
			double complex localsigma=Sigma1(d,omega,n);
		
			sigmas_r[d][c]=creal(localsigma);
			sigmas_i[d][c]=cimag(localsigma);
		}
		
		grid[c]=omega;
	}
	
	/*
		...and from that we initialize the interpolations.
	*/
	
	for(int d=0;d<MAXL;d++)
	{
		sigma_interpolations_r[d]=init_interpolation(grid,sigmas_r[d],DIVS);
		sigma_interpolations_i[d]=init_interpolation(grid,sigmas_i[d],DIVS);
	}

	/*
		Some general information about this run is written on the logfile
	*/

	fprintf(logfile,"# SCBA\n");
	fprintf(logfile,"# Energy cutoff: %f\n",energy_cutoff);
	fprintf(logfile,"# Grid divisions: %i\n",DIVS);
	fprintf(logfile,"# Step size: variable\n");
	fprintf(logfile,"# Iterations: %i\n",max_iterations);
	fprintf(logfile,"# Anderson parameter (alpha): %f\n",ALPHA);
	fprintf(logfile,"# Density, log(n): %f\n",log(n));
	fprintf(logfile,"#\n");
	fflush(logfile);

	/*
		The 0th iteration, which coincides with the usual self-energy, is written
		as a reference in a .dat file 
	*/

	{
		char filename[1024];
	
		snprintf(filename,1024,"%s.it%d.dat",prefix,0);
		filename[1023]='\0';

		dump_tables(filename,energy_cutoff,0,n,sigma_interpolations_r,sigma_interpolations_i);
	}

	/*
		We can start iterating, to find the SCBA
	*/

	for(i=0;(i<max_iterations)&&(earlyexit==0);i++)
	{
		char banner[1024];
		progressbar *progress;

		snprintf(banner,1024,"Iteration #%d",i+1);
		progress=progressbar_new(banner,DIVS);

		/*
			We fill the grid again, this time using the SCBA formula
		*/

#pragma omp parallel for

		for(c=0;c<DIVS;c++)
		{
			double omega=get_omega(c,energy_cutoff);

#pragma omp parallel for

			for(int d=0;d<MAXL;d++)
			{
				double complex localsigma,elocalsigma;

				born_iteration(d,omega,n,sigma_interpolations_r,sigma_interpolations_i,energy_cutoff,&localsigma,&elocalsigma);

#pragma omp critical
				{
					double alpha=ALPHA;
				
					sigmas_r[d][c]=alpha*creal(localsigma)+(1.0f-alpha)*get_point(sigma_interpolations_r[d],omega);
					sigmas_i[d][c]=alpha*cimag(localsigma)+(1.0f-alpha)*get_point(sigma_interpolations_i[d],omega);
				}
			}

#pragma omp critical	
			{
				progressbar_inc(progress);
			}
		}

		progressbar_finish(progress);
		
		/*
			Now we determine a "distance" between the result obtained and that
			of the previous iteration. In case they are really close the routines
			has converged and we can exit the main loop early.
		*/
		
		{
			double rnorm,inorm,rintegral,iintegral;
			
			rnorm=inorm=rintegral=iintegral=0.0f;
		
			for(c=0;c<DIVS;c++)
			{
				int d;
				
				double omega=get_omega(c,energy_cutoff);

				/*
					Note in the next line: d=0;d<=0
				*/
				
				for(d=0;d<=0;d++)
				{
					rnorm+=pow(sigmas_r[d][c]-get_point(sigma_interpolations_r[d],omega),2.0f);
					inorm+=pow(sigmas_i[d][c]-get_point(sigma_interpolations_i[d],omega),2.0f);
				
					rintegral+=pow(sigmas_r[d][c],2.0f);
					iintegral+=pow(sigmas_i[d][c],2.0f);
				}
			}
		
			rnorm=sqrt(rnorm)/sqrt(rintegral);
			inorm=sqrt(inorm)/sqrt(iintegral);

			fprintf(stderr,"# Iteration #%d, norm(L=0) = (%f,%f)\n",i+1,rnorm,inorm);			
			fprintf(logfile,"# Iteration #%d, norm(L=0) = (%f,%f)\n",i+1,rnorm,inorm);

			if((rnorm<0.00001f)&&(inorm<0.00001f))
			{
				fprintf(stderr,"# Early exit due to achieved convergence!\n");
				fprintf(logfile,"# Early exit due to achieved convergence!\n");
			
				earlyexit=1;
			}
			
			fflush(stderr);
			fflush(logfile);
		}

		/*
			From the new grids we create the new interpolations.
		*/
		
		for(int d=0;d<MAXL;d++)
		{
			fini_interpolation(sigma_interpolations_r[d]);
			fini_interpolation(sigma_interpolations_i[d]);
			
			sigma_interpolations_r[d]=init_interpolation(grid,sigmas_r[d],DIVS);
			sigma_interpolations_i[d]=init_interpolation(grid,sigmas_i[d],DIVS);
		}

		/*
			And finally we write the data files for this iteration
		*/

		{
			char filename[1024];
		
			snprintf(filename,1024,"%s.it%d.dat",prefix,i+1);
			filename[1023]='\0';
			
			dump_tables(filename,energy_cutoff,i+1,n,sigma_interpolations_r,sigma_interpolations_i);
		
			last_iteration=i;
		}
	}

	/*		
		Finally we copy the last valid file to scba.nxxx.dat, i.e. without it "it" specifier.
	*/

	{
		char in[1024],out[1024];
		FILE *fin,*fout;
			
		snprintf(in,1024,"%s.it%d.dat",prefix,last_iteration+1);
		in[1023]='\0';

		snprintf(out,1024,"%s.dat",prefix);
		out[1023]='\0';

		if((fin=fopen(in,"r")))
		{
			if((fout=fopen(out,"w+")))
			{
				fcopy(fin,fout);
				fclose(fout);
			}
		
			fclose(fin);
		}
	}

	return 0;
}

/*
	Given a target value targetsigma, the present function returns
	an energy cutoff value such that the self-energy is never larger
	than targetsigma value outside the interval [-cutoff,+cutoff].

	More practically: if we are not interested in areas where the self-energy
	is smaller than a certain value, this function will find an adequate cutoff.
*/

double find_cutoff(short L,double n,double targetsigma)
{
	double omegalo,omegahi,omegamid,omega;
	double sigmalo,sigmahi,sigmamid;
	
	short c,maxiter=32;

	omegalo=-128.0f*1024.0f*1024.0f;
	omegahi=-15.0f;

	sigmalo=creal(Sigma1(L,omegalo,n))-targetsigma;
	sigmahi=creal(Sigma1(L,omegahi,n))-targetsigma;

	if((sigmalo*sigmahi)>=0.0f)
	{
		fprintf(stderr,"Initial values do not bracket a root (sigmalo=%f, sigmahi=%f).\n",sigmalo,sigmahi);
		return -1;
	}

	for(c=0;c<maxiter;c++)
	{		
		omegamid=(omegahi+omegalo)/2.0f;
		sigmamid=creal(Sigma1(L,omegamid,n))-targetsigma;

		if(sigmamid==0.0f)
			break;

		if((sigmamid*sigmalo)<=0.0f)
		{
			omegahi=omegamid;
			sigmahi=sigmamid;
		}
	
		else if((sigmamid*sigmahi)<=0.0f)
		{
			omegalo=omegamid;
			sigmalo=sigmamid;
		}
		
		else
		{
			fprintf(stderr,"Error in rootfinding algorithm.\n");
			return -1;
		}
	}

	omega=omegamid;
	
	return fabs(omega);
}

/*
	Find the maximum value of the self-energy Sigma1,
	over the whole energy range, given L and n.
*/


double golden_section(double a,double c,double b,double tau,short L,double n)
{
	double phi=(1.0f+sqrtf(5.0f))/2.0f;
	double resphi=2.0f-phi;
	double x;

	if(fabs(a-b)<tau)
		return (a+b)/2.0f;

	x=c+resphi*(b-c);
	
	if(fabs(creal(Sigma1(L,x,n)))<fabs(creal(Sigma1(L,b,n))))
		return golden_section(c,x,b,tau,L,n);

	return golden_section(x,c,a,tau,L,n);
}

double find_maximum(short L,double n)
{
	double a,b,c,omega0,maximum1,maximum2,val1,val2;

	/*
		The golden section algorithm requires the function to be unimodal,
		and Sigma1 is not. However it has a zero, let's say at omega0, and
		it will be unimodal in the intervals [-infinity,omega0] and [omega0,+infinity].
	
		We start by bracketing the zero of Sigma1 using an ultrasimple bracketing algorithm...
	*/

	a=-5000.0f;
	b=5000.0f;
	c=0.0f;

	for(short iters=0;iters<=32;iters++)
	{
		if(creal(Sigma1(L,b,n))*creal(Sigma1(L,c,n))<0.0f)
		{
			a=b;
			b=(a+c)/2.0f;
		}
		else
		{
			c=b;
			b=(a+c)/2.0f;
		}
	}

	omega0=b;

	/*
		...and then we run the golden section search on the two intervals.
	*/

	maximum1=golden_section(250.0f,omega0,(omega0+250.0f)/2.0f,0.0001f,L,n);
	maximum2=golden_section(-250.0f,omega0,(omega0-250.0f)/2.0f,0.0001f,L,n);

	/*
		Finally, we return the largest peak found.
	*/

	val1=fabs(creal(Sigma1(L,maximum1,n)));
	val2=fabs(creal(Sigma1(L,maximum2,n)));

	return (val1>val2)?(val1):(val2);
}

int main(void)
{
	double logn;
	char prefix[1024],filename[1024],message[1024];
	short cnt=0;

	curl_global_init(CURL_GLOBAL_DEFAULT);

	/*
		Let's init Pushover with the API token for this application
	*/

	cpsh_init("apcgnwr28x744xatein6z9b5v18syr");

	for(logn=5;logn>=-15;logn-=0.5,cnt++)
	{
		double targetsigma,cutoff;
		FILE *logfile;

		snprintf(prefix,1024,"scbafiner2.n%d",cnt);
		prefix[1023]='\0';
		
		snprintf(filename,1024,"%s.log",prefix);
		filename[1023]='\0';

		if(!(logfile=fopen(filename,"w+")))
			continue;

		printf("Writing output to %s. (cnt=%d,logn=%f)\n",filename,cnt,logn);

		/*
			The cutoff is chosen so that we "ignore" the region
			in which the self-energy is smaller than the value
			of the highest peak divided by x.
		
			It turns out that when log(n)>0 a quick convergence
			is achieved by using x=125.0. At lower densities, on
			the other hand, x=50.0 gives good results.
		*/

		if(logn>0.0f)
			targetsigma=(-1.0f)*find_maximum(0,exp(logn))/125.0f;
		else
			targetsigma=(-1.0f)*find_maximum(0,exp(logn))/50.0f;

		targetsigma=(-1.0f)*find_maximum(0,exp(logn))/50.0f;
		cutoff=find_cutoff(0,exp(logn),targetsigma);

		scba(exp(logn),8,cutoff,prefix,logfile);

		if(logfile)
			fclose(logfile);
	
		snprintf(message,1024,"Calculation completed for log(n)=%f",logn);
		message[1023]='\0';

		{
			cpsh_message msg;
			memset(&msg,0,sizeof(msg));
			
			/*
				The first field is Pushover's user key
			*/
			
			msg.user="uUK6JaCXb3rKCuBfTgg8p5fkaLvKw5";
			msg.message=message;
			
			cpsh_send(&msg);
		}
	}

	curl_global_cleanup();

	return 0;
}

