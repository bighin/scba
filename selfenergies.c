#include "selfenergies.h"
#include <gsl/gsl_sf.h>

/*
	We use the Cubature library for numerical integration,
	see http://ab-initio.mit.edu/wiki/index.php/Cubature
*/

#include "cubature-1.0.2/cubature.h"

/*
	Nice progress bar
*/

#include "libprogressbar/progressbar.h"

/*
    GCC does not define M_PI in C11 mode
*/

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif

/*
	Tuning paramaters for numerical integration, defined in spectral.c for convenience.
*/

extern size_t maxEval;
extern double relError;

/*
	Physical parameters, as in R. Schmidt and M. Lemeshko, Phys. Rev. Lett. 114, 203001 (2015).
*/

double u0=218;
double u1=218.0f/1.75f;

double r0=1.5;
double r1=1.5;

double abb=3.3;

/*
	Epsilon, also defined in spectral.c
*/

extern double epsilon;

/*
	Dawson's integral, from: http://www.ebyte.it/library/codesnippets/DawsonIntegralApproximations.html
        (maximum relative error: 10 ppm)
*/

double DawsonF(double x)
{
	double y,p,q;

	y = x*x;

	p = 1.0 + y*(0.1049934947 + y*(0.0424060604
                + y*(0.0072644182 + y*(0.0005064034
                + y*(0.0001789971)))));

	q = 1.0 + y*(0.7715471019 + y*(0.2909738639
                + y*(0.0694555761 + y*(0.0140005442
                + y*(0.0008327945 + 2*0.0001789971*y)))));

	return x*(p/q);
}

/*
	Wigner's 3j symbol from the GNU Scientific Library and the F function
	we get at every vertex in terms of the 3j symbol
*/

double wigner3j(int l1,int l2,int l3,int m1,int m2,int m3)
{
	return gsl_sf_coupling_3j(2*l1,2*l2,2*l3,2*m1,2*m2,2*m3);
}

/*
	Dispersion relations for free particles and Bogoliubov quasiparticles
*/

double ek(double k)
{
	return k*k/2.0f;
}

double omegak(double k,double n)
{
	return sqrt(ek(k)*(ek(k)+8.0f*M_PI*abb*n));
}

/*
	Potentials
*/

double U0(double n,double k)
{
	double a,b,c;

	a=sqrt((8.0f*n*k*k*ek(k))/(omegak(k,n)));
	b=exp(-0.5f*k*k*r0*r0);
	c=4*M_PI*pow(r0,-3.0f);

	return u0*a*b/c;
}

double U1(double n,double k)
{
	double a,b,c;

	a=sqrt((8.0f*n*k*k*ek(k))/(3.0f*omegak(k,n)));
	b=r1*(-sqrt(2.0f)*k*r1+2.0f*(1+k*k*r1*r1)*DawsonF(k*r1/sqrt(2.0f)));
	c=4*pow(M_PI,3.0f/2.0f)*k*k;
	
	return u1*a*b/c;
}

double U(short lambda,double n,double k)
{
	switch(lambda)
	{
		case 0:
		return U0(n,k);

		case 1:
		return U1(n,k);
	
		default:
		fprintf(stderr,"U() called with wrong argument lambda=%d\n",lambda);
		exit(0);
	}
}

/*
	Self-energies, first order
*/

struct sigma_ctx_t
{
	double omega,n,cg2;

	short L,lambda,j;
};

int fSigma1(unsigned ndim,const double *x,void *fdata,unsigned fdim,double *fval)
{
	/* The context from which we read the global variables */
	
	struct sigma_ctx_t *ctx=(struct sigma_ctx_t *)(fdata);

	/* Global variables */

	short lambda,j;
	double omega,n,cg2;

	/* The integration variables */

	double t=x[0];
	double k=t/(1.0f-t);

	/* Auxiliary variables */
	
	double complex a,b,c,z;

	/* We load the global variables */

	omega=ctx->omega;
	n=ctx->n;
	cg2=ctx->cg2;

	lambda=ctx->lambda;
	j=ctx->j;

	/* Finally, we compute the result and return it */

	a=(2.0f*lambda+1)/(4.0f*M_PI);
	b=pow(U(lambda,n,k),2.0f)*cg2;
	c=j*(j+1)-(omega+I*epsilon)+omegak(k,n);

	z=(-1.0f)*a*b/c*pow(1-t,-2.0f);
	
	fval[0]=creal(z);
	fval[1]=cimag(z);

	return 0;
}

double complex Sigma1(short L,double omega,double n)
{
	double xmin[1]={0.0f};
	double xmax[1]={1.0f};
	double res[2],err[2];

	double complex total;

	struct sigma_ctx_t ctx;

	ctx.L=L;
	ctx.n=n;
	ctx.omega=omega;

	total=0.0f;

	for(ctx.lambda=0;ctx.lambda<=1;ctx.lambda++)
	{
		for(ctx.j=0;ctx.j<=L+1;ctx.j++)
		{
			double cg2=pow(wigner3j(ctx.j,L,ctx.lambda,0,0,0),2.0f)*(2*ctx.j+1);
			
			if(fabs(cg2)>1e-7)
			{
				ctx.cg2=cg2;
				
				hcubature(2,fSigma1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
				total+=(res[0]+I*res[1]);
			}
		}
	}

	return total;
}

