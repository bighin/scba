#ifndef __SELF_ENERGIES_H__
#define __SELF_ENERGIES_H__

#include <math.h>
#include <complex.h>

double wigner3j(int l1,int l2,int l3,int m1,int m2,int m3);

double U(short lambda,double n,double k);
double omegak(double k,double n);

double complex Sigma1(short L,double omega,double n);

#endif //__SELF_ENERGIES_H__

